const Category = new (require('./database/other/Category'))
    , paginating = require('./jobs/paginating')
    , scarpingCategory = require('./jobs/scarpingCategory');

module.exports = class App {

    constructor(maxJobsPaginating) {
        this.maxJobsPaginating = maxJobsPaginating;
    }

    async start() {
        await Category.init();

        let scarpCategory = scarpingCategory.createJob('scarpingCategory');

        scarpCategory.on('complete', async (result) => {

            let links = await Category.getLinks(this.maxJobsPaginating);

            this.createJobPaginating(links);

        }).on('failed attempt', (errorMessage, doneAttempts) => {
            console.log('Job failed');
        });
    }

    createJobPaginating(links) {
        for (let i = 0; i < links.length; i++) {
            paginating.createJob(
                'paginating',
                {link: links[i].link, id: links[i].id}
            ).on('complete', async (result) => {
                await this.startJobPaginating(result, 'complete');
            }).on('error', async (errorMessage) => {
                await this.startJobPaginating(errorMessage.id, 'error');
            });
        }
    }

    async startJobPaginating(result, status) {
        await Category.updateCatStatus(result, status);

        let link = await Category.getLinks(1);

        this.createJobPaginating(link);
    }
};