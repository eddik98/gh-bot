const argv = require('yargs')
    .alias('p', 'provider')
    .describe('p', 'Список доступных провайдеров')
    .choices('p', ['DNS', 'Mobex', 'Park', 'SotaCrimea'])

    .alias('env', 'environment')
    .describe('env', '')
    .choices('env', ['dev-goods', 'dev-advert', 'prod-goods', 'prod-advert', 'dev-goods-prodDB'])

    .alias('s', 'status')
    .describe('s', 'Продолжить ли работу?')
    .choices('s', ['True', 'False'])
    .help('help')
    .argv;

process.env.Provider = (argv.p !== undefined) ? argv.p : process.exit();
process.env.NODE_ENV = (argv.env !== undefined) ? argv.env : process.exit();
process.env.ResumeWork = (argv.s !== undefined) ? argv.s : 'False';

const saveDataElement = require('../jobs/saveDataElement');

require('longjohn');

const cluster = require('cluster')
    , clusterWorkerSize = 4;

if (cluster.isMaster) {

    for (let i = 0; i < clusterWorkerSize; i++)
        cluster.fork();


} else {
    saveDataElement.Queue.process('saveDataElement', 4, saveDataElement.work);
}