const l = require('../../config/logger').getLogger('elementScarperXRay');
const DataProvider = require('./../../dataProvider/ManagerProvider').get();

const Xray = require('x-ray');
let x = Xray({
    filters: {
        trim: function (value) {
            return typeof value === 'string' ? value.trim() : value
        },
        replace: function (value) {
            return typeof value === 'string' ? value.replace(/\s+/g, ' ') : value
        },
        onlyNumbers: function (value) {
            return typeof value === 'string' ? value.replace(/(\W+)/g, '') : value
        },
        sanitize: function (value) {
            return typeof value === 'string' ?
                value.replace(/[&\/\\#,+()$~%.`'"!:*?<>{}]/g, '') : value
        }
    }
});

class XRayScarper {

    /**
     * Запуск парсинга данных элемента
     *
     * @param {array} links
     * @param {function} cBackKue
     * @return {boolean, Error}
     */
    async start(links, cBackKue) {
        let result;
        let nowAttempts = 0;
        let attempts = 2;
        let totalAttempts = 0;

        for (let i = 0; i < links.length; i++) {
            try {
                result = await x(links[i], DataProvider.getInfo(x));

                if (result.goods.hasOwnProperty('name') === false)
                    throw new Error('Result scraping is empty: |' + links[i]);

                result.goods.link = links[i];

                cBackKue(result, i, links.length);
            } catch (e) {

                if (nowAttempts !== attempts) {
                    l.error(e);
                    l.info('Попытка:', nowAttempts + 1, links[i]);
                    i--;
                    nowAttempts++;
                } else if (nowAttempts === attempts) {
                    totalAttempts++;
                    nowAttempts = 0;
                }

                if (totalAttempts === attempts)
                    throw e;

            }
        }
        return true;
    }
}


module.exports = XRayScarper;