const DataProvider = require('./../../../dataProvider/ManagerProvider').get();
const Main = new(require('./../../../database/main/Main'));
const Helper = new(require('./../../../helper'));

module.exports = class Scarper {

    constructor() {
        this.dp = DataProvider;
        this.url = DataProvider.url;
    }

    validateUrls(arr) {
        let tmp = [];

        arr = Helper.removeDuplicates(arr);

        arr.forEach((elem) => {
            elem = Helper.is_url(elem) ? elem : this.url + elem;
            tmp.push(elem);
        });

        return tmp;
    }

    async saveToDB(list) {
        try {
            let providerId = await DataProvider.getId();

            let save = async (link) => {
                await Main.m.ListCategory.findOrCreate({
                    where: {
                        link: link,
                        provider_id: providerId
                    },
                    defaults: {
                        link: link,
                        provider_id: providerId
                    },
                    attributes: ['id']
                });
            };

            await Helper.multithreading(list, save);
        } catch (e) {
            throw e;
        }
    }
};
