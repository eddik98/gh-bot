const request = require("request"),
    cheerio = require("cheerio");

module.exports = class Cheerio extends require('./main/Scarper') {

    constructor() {
        super();

        this.userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 ' +
            '(KHTML, like Gecko) Chrome/67.0.3396.79 Safari/537.36';
    }

    async start() {
        let $ = await this.get$();
        let tmp;

        tmp = this.dp.scarperCategory($);

        tmp = this.validateUrls(tmp);

        await this.saveToDB(tmp);

        return true;
    }

    getHtml() {
        return new Promise((resolve) => {
            request(this.url, {
                headers: {'User-Agent': this.userAgent}
            }, (error, response, body) => {
                if (!error)
                    resolve(body);
                else
                    console.log("Произошла ошибка: " + error);
            });
        });
    }

    getCheerio(html) {
        return cheerio.load(html);
    }

    async get$() {
        let html = await this.getHtml();
        return this.getCheerio(html);
    }
}
