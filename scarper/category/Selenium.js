const Driver = new (require('../../paginate/Selenium/initSelenium'));

module.exports = class Selenium extends require('./main/Scarper') {

    constructor() {
        super();
        this.driver = Driver.getDriver();
    }

    async start() {
        let tmp;

        tmp = await this.getLinks(this.url, this.dp.scarperCategory());

        tmp = this.validateUrls(tmp);

        await this.saveToDB(tmp);

        return true;
    }


    async getLinks(url, script) {
        await this.driver.get(url);

        let links = await this.driver.executeScript(script);

        await this.driver.quit();

        return links;
    }
};
