const DataProvider = require('./../dataProvider/ManagerProvider').get();

module.exports = class ManagerScarper {

    static get element() {
        return new(require('./element/' + DataProvider.scarper.element));
    }

    static get category() {
        return new(require('./category/' + DataProvider.scarper.category));
    }

};