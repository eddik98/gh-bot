const DataProvider = new (require('./providers/' + process.env.Provider));

module.exports = class ManagerProvider {

    static get() {
        return DataProvider;
    }

};

