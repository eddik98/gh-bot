let helper = require('../../../helper');
class Optomobex {
  get url() {
    return 'http://optmobex.ru';
  }

  constructor(phantom) {
    this.scopeCategories = '.main-menu-tablet-desktop > .main-menu-opened-container';
    this.currentPage = 1;
    this.urlGoods = [];
  }

  getTotalPages() {
    return function() {
      return $('.module-pagination > .nums').children().last().text();
    }
  }

  setUrl(url) {
    this._url = url;
  }

  pageIsLoaded() {
    return function() {
      return true;
    }
  }

  changePage() {
    return function(page) {
      var pages = $('.module-pagination > .nums').children();
      for (var i = 0; i < pages.length; i++)
        if ($(pages[i]).text() == page)
          $(pages[i]).click();


      var start = Date.now(),
        now = start;
      while (now - start < 500) {
        now = Date.now();
      }
      return true;
    }
  }


  getGoods(cBack) {
    return function() {
      var result = [];
      var children = $('.catalog-items-list').children();
      for (var i = 0; i < Object.keys(children).length; i++) {
        var e = $(children[Object.keys(children)[i]]);
        var href = e.find('.title a').attr('href');
        result.push({
          link: href
        });
      }
      return result;
    }
  }

  getCategories() {
    return ['a@href'];
  }

  getInfoGoods(x, link) {
    return {
      goods: {
        name: 'h1#page-title',
        price: '.price_g > span@data-price-value',
        description: '.preview_text.dotdot',
        link: 'meta[property="og:url"]@content',
        meta_description: 'meta[name="description"]@content',
        sku: 'div.article.iblock > span:nth-child(2)',
        meta_key: 'meta[name="keywords"]@content',
        meta_description: 'meta[property="og:description"]@content',
      },
      goods_image: x('.slides > ul > li > a', [{
        link: 'img@src'
      }]),
      category: {
        name: 'div.breadcrumbs > .bx-breadcrumb-item:nth-child(3) > span > span'
      },
    }
  }
}
module.exports = Optomobex;