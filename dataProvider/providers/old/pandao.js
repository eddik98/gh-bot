let helper = require('../../../helper');
const {
  Builder,
  By
} = require('selenium-webdriver');

class Pandao {
  get url() {
    return 'https://pandao.ru';
  }

  getTotalPages() {
    return function() {
      return 1;
    };
  }

  setUrl(url) {
    let self = this;
    this._url = url;
    new Builder().forBrowser('firefox').build().then(driver => {
      driver.get(url).then(() => {
        self._driver = driver;
      });
    });
  }

  constructor(phantom) {
    this.currentPage = 1;
    this.scopeCategories = '.main-menu-tablet-desktop > .main-menu-opened-container';
    this.urlGoods = [];
    this.goodsData = [];
  }

  pageIsLoaded(cBack) {
    if (this._driver && !this._lockClicker) {
      this._lockClicker = true;
      this._driver.findElements(By.id('loader')).then((btn) => {
        btn[0].getCssValue('display').then((display) => {
          if (display === 'block') {
            btn[0].click();
            this._lockClicker = false;
          } else cBack();
        });
      });
    }
    return function() {
      return false;
    };
  }

  changePage() {

  }

  getGoods(cBack) {
    this._driver.findElement(By.className('product-item-wrapper'))
      .findElements(By.className("product-item")).then((elements) => {
        let result = [];
        helper.asyncLoop(elements.length, loop => {
          elements[loop.iteration()].getAttribute('href').then(href => {
            result.push({
              link: href
            });
            loop.next();
          });
        }, () => {
          cBack(result);
          this._driver.quit();
          this._driver = null;
        });
      });
    return function() {
      return 'SKIP';
    }
  }

  getCategories() {
    return ['a@href'];
  }

  //Сделать как в ДНС!!!!!!!!!!
  getInfoGoods(x) {
    return {
      goods: {
        name: '.product-title[itemprop="name"] | replace | trim',
        price: '.wrapper-value-price > p[itemprop="price"] | onlyNumbers',
        description: '.product-description-body | replace',
        link: 'meta[property="og:url"]@content',
        //meta_key: 'meta[property="og:title"]@content',
        meta_description: 'meta[name="description"]@content',
      },
      product_image: x('meta[property="og:image"]', [{
        link: '@content'
      }]),
      category: {
        name: '.breadcrumbs > span:last-child | replace | trim'
      },
    }
  }

}

module.exports = Pandao;