class MadRobot {
  get url() {
    return 'http://madrobots.ru';
  }

  constructor(phantom) {
    this.currentPage = 1;
    this.scopeCategories = '.category:nth-child(1) > .category__item';
    this.urlGoods = [];
    this.currency = 'RUB';
    this.goodsData = [];
  }

  getTotalPages() {
    return function() {
      var cnt = $($('.pagination')[0]).children().length;
      return cnt === 0 ? 1 : cnt;
    }
  }

  pageIsLoaded() {
    return function() {
      return $('.page-loader').length === 0;
    }
  }

  changePage() {
    return function(page) {
      var p = $($('.pagination')['0']).children()[page - 1];
      if (p)
        p.click();
      var start = Date.now(),
        now = start;
      while (now - start < 500) {
        now = Date.now();
      }
      return true;
    }
  }

  getGoods() {
    return function() {
      var result = [];
      var children = $('.catalog-wrap').children();
      for (var i = 0; i < Object.keys(children).length; i++) {
        // children[Object.keys(children)[i]]
        var e = $(children[Object.keys(children)[i]]);
        var href = e.find('.catalog-grid__item__name').attr('href');
        result.push({
          link: href
        });
      }
      return result;
    }
  }


  getCategories() {
    return ['a@href'];
  }

  //Сделать как в ДНС!!!!!!!!!!
  getInfoGoods(x) {
    return {
      goods: {
        name: 'h1.product__name > span:nth-child(1)',
        price: '.product__description--seo > strong:nth-child(2) | onlyNumbers | replace',
        description: '.product__description-text | replace',
        link: 'meta[property="og:url"]@content',
        meta_key: 'meta[property="og:title"]@content',
        meta_description: 'meta[name="description"]@content',
      },
      dict_property: x('.product__characteristics__tr', [
        '.product__characteristics__td:nth-child(1)'
      ]),
      product_property: x('.product__characteristics__tr', [
        '.product__characteristics__td:nth-child(2)'
      ]),
      product_image: x('.product__img--big > a', [{
        link: '@href'
      }]),
      category: {
        name: '.breadcrumbs > li:nth-child(3)'
      },
    }
  }
}

module.exports = MadRobot;