/* eslint-disable indent,class-methods-use-this,no-var */
const fs = require('fs');

class AdCrimea {

    get url() {
        return 'https://reklama-crimea.com';
    }

    get name() {
        return 'Реклама Крыма';
    }

    constructor(phantom) {
        this.scopeCategories = '.main-menu-tablet-desktop >' +
            ' .main-menu-opened-container';
        this.currentPage = 1;
        this.urlGoods = [];
        this.prefix = '?page=';
    }

    getListCategories() {
        let texts = fs.readFileSync("./list_category/mollva/linksAll-r.txt", "utf-8");
        let text = texts.split("\n");
        return text;
    }

    getTotalPages() {
        return function () {
            var t = $('ul.pagination > ul').children().length;
            var t2 = $($('ul.pagination > ul').children())[t - 2];
            var totalPages = $(t2).find('a').text();

            if(totalPages === '')
                totalPages = 0;

            return totalPages;
        };
    }

    setUrl(url) {
        this.urls = url;
    }

    pageIsLoaded() {
        return function () {
            var is = $('.ad-item-holder').last().ready();
            if (is.length > 0) {
                return true;
            } else {
                return false;
            }
        };
    }

    changePage() {
        return function (page) {
            if (page !== 1) {
                var pages = $('.pagination > ul').find('li.active');
                var t = $($(pages)[0].nextElementSibling).find('a');
                $(t[0])[0].click();
            }

            var start = Date.now(),
                now = start;
            while (now - start < 12000) {
                now = Date.now();
            }
            return true;
        };
    }

    getGoods() {
        return function () {
            var result = [];
            var children = $('.advts-holder').children();
            if (children.length !== 0) {
                for (var i = 0; i < Object.keys(children).length; i++) {
                    var e = $(children[i]);
                    var href = e.find('.content > table > tbody > tr > td > h3 > a').attr('href');
                    if (href !== undefined) {
                        result.push(href);
                    }
                }
            }

            var date = $($($('.published.author-name').last()).find('p')[0]).text();

            return {date: date, result: result};
        }
    }

    getCategories() {
        return ['a@href'];
    }

    getInfoGoods(x) {
        return {
            goods: {
                name: '.ad-item-details > h1',
                price: '.price > span | onlyNumbers',
                description: '.description | replace | trim',
                meta_description: 'meta[name="og:description"]@content | replace | trim',
                telephone: 'a.author_phone',
                link: 'meta[name="og:url"]@content',
                city: '.contact-info > table > tr:nth-child(4) > td:nth-child(2)',
                address: '.contact-info > table > tr:nth-child(5) > td:nth-child(2) > a',
                author: '.title-block > h3'
            },
            property: x('div.ad-item-details > p.advt_additional_info_holder', [{
                value: '@text',
            }]),
            goods_image: x('.slideshow-block > #mycarousel2 > li', [{
                link: 'img@data-big',
            }]),
            category: x('.breadcrumbs > li > a', [{
                name: 'span',
            }]),

        };
    }
}

module.exports = AdCrimea;