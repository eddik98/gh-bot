class SotaCrimea extends require('./main/UrlProvider') {
    constructor() {
        super();
        this.name = 'Sota Crimea';
        this.url = 'https://sota-crimea.ru';
        this.urlPrefix = 'page=';
        this.urlParams = '';
        this.scarper = {element: 'XRay', category: 'Cheerio'};
        this.paginator = 'Hybrid';
    }

    getTotalPages() {
        return function () {
            let totalPage = $('.menu-h').children().length;
            return (totalPage <= 1) ? totalPage : totalPage - 1;
        }
    }

    getListLinks(x) {
        return x('.thumbs.product-list > li > a:nth-child(1)', [
            '@href'
        ]);
    }

    scarperCategory($) {
        let tmp = [];
        $('.menu-v.brands').children().each((i, elem) => {
            let data = $(elem).find('a').attr('href');
            if (data)
                tmp.push(data);
        });

        $('.menuline > .container > .flyout-nav').children().each((i, elem) => {
            let data = $(elem).find('a').attr('href');
            if (data)
                tmp.push(data);
        });

        return tmp;
    }

    getInfo(x) {
        return {
            goods: {
                name: 'span[itemprop="name"]',
                price: '.add2cart > span@data-price',
                description: '#product-description',
                meta_key: 'meta[name="Keywords"]@content',
                meta_description: 'meta[name="Description"]@content',
                rating: 'span[itemprop="ratingValue"]'
            },
            dict_property: x('.features > tr', [
                'td.name | trim'
            ]),
            goods_property: x('.features > tr', [
                'td.value | trim'
            ]),
            goods_image: x('.more-images > .image > a', [
                '@href'
            ]),
            category: x('.breadcrumbs > a', [
                '@'
            ])
        }
    }

}

module.exports = SotaCrimea;