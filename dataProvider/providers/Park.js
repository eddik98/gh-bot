class Mobex extends require('./main/UrlProvider') {

    constructor() {
        super();
        this.name = 'Park';
        this.url = 'https://park-mobile.ru';
        this.urlPrefix = 'PAGEN_1=';
        this.urlParams = 'VIEW=LINE';
        this.scarper = {element: 'XRay', category: 'Cheerio'};
        this.paginator = 'Hybrid';
    }

    getTotalPages() {
        return function () {
            var pages = $('.bx-pagination-container ul li a span');
            return $(pages[pages.length - 2]).text();
        }
    }

    getListLinks(x) {
        return x('#catalogLineList > .itemRow > .column:nth-child(2) > a', [
            '@href'
        ]);
    }

    scarperCategory($) {
        let tmp = [];
        let menu = $('.collapsed > #leftMenuNew').children();

        menu.each((i, elemMenu) => {
            tmp.push($(elemMenu).find('a').attr('href'));
            //console.log($(elemMenu).find('.drop_block > ul'));
            $(elemMenu).find('.drop_block > ul').children().each((i, elemSubMenu) => {

                tmp.push($(elemSubMenu).find('a').attr('href'));

                $(elemSubMenu).find('ul').children().each((i, elemSubsMenu) => {
                    tmp.push($(elemSubsMenu).find('a').attr('href'));
                });
            });
        });

        return tmp;
    }

    getInfo(x) {
        return {
            goods: {
                name: 'h1.changeName',
                price: '.changePrice | trim | onlyNumbers | replace',
                description: '.changeDescription',
                meta_description: 'meta[name="description"]@content',
                meta_key: 'meta[name="keywords"]@content'
            },
            dict_property: x('#elementProperties > .stats > tbody > tr:not(.cap)', [
                'td:nth-child(1) | trim'
            ]),
            goods_property: x('#elementProperties > .stats > tbody > tr:not(.cap)', [
                'td:nth-child(2) | trim'
            ]),
            goods_image: x('.slideBox > div.item', [
                'a@href'
            ]),
            category: x('#breadcrumbs > ul > li > a', [
                '@'
            ])
        };
    }
}

module.exports = Mobex;