class DNS extends require('./main/UrlProvider') {

    constructor() {
        super();
        this.name = 'DNS Shop';
        this.url = 'https://www.dns-shop.ru';
        this.urlPrefix = 'p=';
        this.urlParams = 'a=1';
        this.scarper = {element: 'XRay', category: 'Selenium'};
        this.paginator = 'Hybrid';
    }

    getTotalPages() {
        return function () {
            return $($('.pagination .edge')[1]).attr('data-page-number');
        }
    }

    getListLinks(x) {
        return x('.catalog-items-list > .item > .catalog-item-inner > ' +
            '.product-info > .title > a', [
            '@href'
        ]);
    }

    scarperCategory() {
        return () => {
            var tmp = [];
            let menu = $('ul#menu-catalog > li > .sub-wrap > .catalog-subcatalog');
            menu.each((i, elemMenu) => {
                //catalog-subcatalog level-1 > (all) li
                let chidMenu = $(elemMenu).children();
                //Проходимся по каждому li chidMenu
                chidMenu.each((i, elemSubMenu) => {
                    //Получаем детей .item-wrap и .catalog-subcatalog level-2
                    let chidSubMenu = $(elemSubMenu).children();
                    //Если есть элемент с классом level-2
                    if ($(chidSubMenu).hasClass('level-2')) {
                        //Берем последнего ребенка, то есть элемент catalog-subcatalog level-2
                        //И проходимся по всем li
                        $(chidSubMenu).last().each((i, elemSub) => {
                            //Получаем детей .item-wrap и .catalog-subcatalog level-3
                            let chidSubSub = $(elemSub).children();
                            //Если есть элемент с классом level-3
                            $(chidSubSub).each((i, elem) => {
                                let chid = $(elem).children();
                                if ($(chid).hasClass('level-3')) {
                                    $(chid).last().children().each((ind, el) => {
                                        tmp.push($(el).find('.item-wrap > a').attr('href'));
                                    });
                                } else {
                                    //Берем только .item-wrap > a@href
                                    tmp.push(chid[0].children[0].href);
                                }
                            });
                        });
                    } else {
                        //Берем только .item-wrap > a@href
                        tmp.push(chidSubMenu[0].children[0].href);
                    }
                });
            });
            return tmp;
        };
    }

    getInfo(x) {
        return {
            goods: {
                name: 'h1.page-title',
                price: '.price_g > span@data-price-value',
                description: '.price-item-description > p',
                meta_description: 'meta[name="description"]@content',
                rating: 'span[itemprop="ratingValue"]'
            },
            dict_property: x('.table-params > tr', [
                'td:nth-child(1) > .dots > span'
            ]),
            goods_property: x('.table-params > tr', [
                'td:nth-child(2)'
            ]),
            goods_image: x('#mainImageSliderWrap > div.image-slider > div.img > a', [
                'img@src'
            ]),
            category: x('.product-breadcrumbs:nth-child(2) > .breadcrumb > li > a', [
                'span'
            ])
        }
    }
}

module.exports = DNS;