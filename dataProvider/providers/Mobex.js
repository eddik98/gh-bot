class Mobex extends require('./main/UrlProvider') {

    constructor() {
        super();
        this.name = 'Mobex';
        this.url = 'https://mobex.com.ru';
        this.urlPrefix = 'PAGEN_1=';
        this.urlParams = 'display=list&MOBEX_USERS_GROUP=0';
        this.scarper = {element: 'XRay', category: 'Cheerio'};
        this.paginator = 'Hybrid';
    }

    getTotalPages() {
        return function () {
            return $('.module-pagination > .nums').children().last().text();
        }
    }

    getListLinks(x) {
        return x('.display_list > .list_item_wrapp > .list_item > .adaptive_name > td > .desc_name > a', [
            '@href'
        ]);
    }

    scarperCategory($) {
        let tmp = [];

        let menu = $('ul.menu.dropdown').children();
        menu.each(function (i, elemMenu) {
            let chidMenu = $(elemMenu).children().last();

            if (chidMenu.is('ul')) {
                $(chidMenu).children().each(function (i, elemSubMenu) {
                    let data = $(elemSubMenu).find('a').attr('href');
                    if (data)
                        tmp.push(data);
                });
            } else {
                let data = $(chidMenu).attr('href');
                if (data)
                    tmp.push(data);
            }
        });

        return tmp;
    }

    getInfo(x) {
        return {
            goods: {
                name: 'h1#pagetitle',
                price: '.cost.prices > div > span:nth-child(1) | onlyNumbers | trim',
                description: '.tabs_section > ul.tabs_content > li:nth-child(1) > div | trim',
                meta_description: 'meta[name="description"]@content',
                rating: 'meta[itemprop="ratingValue"]@content',
                meta_key: 'meta[name="keywords"]@content',
            },
            dict_property: x('.tabs_section > ul.tabs_content >' +
                ' li:nth-child(2) > div > table > tbody > tr.prop_line', [
                'td:nth-child(1) | replace | trim'
            ]),
            goods_property: x('.tabs_section > ul.tabs_content >' +
                ' li:nth-child(2) > div > table > tbody > tr.prop_line', [
                'td:nth-child(2) | replace | trim'
            ]),
            goods_image: x('.slider-wrap > .slider > li', [
                'img@src'
            ]),
            category: x('.breadcrumbs > .bx-breadcrumb-item', [
                'span[itemprop="name"]'
            ])
        };
    }
}

module.exports = Mobex;