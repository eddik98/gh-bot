const MainDB = new (require('./../../../database/main/Main'));

module.exports = class Provider {

    constructor() {
        this.id = null;
    }

    getName() {
        return this.name;
    }

    getUrl() {
        return this.url;
    }

    async getId() {
        if (this.id === undefined || this.id === null)
            this.id = await MainDB.getOrSetProvider(this.name, this.url);

        return this.id
    }

};