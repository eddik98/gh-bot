class UrlProvider extends require('./Provider') {
    constructor() {
        super();
    }

    getUrlPrefix(){
        return this.urlPrefix;
    }

    getUrlParams(){
        return this.urlParams;
    }
}

module.exports = UrlProvider;