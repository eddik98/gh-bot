const log4js = require('log4js');
const lvl = 'debug';
const log = log4js.configure({
    appenders: {
        out: {type: 'stdout'},
        app: {type: 'stdout'},
        paginator: {type: 'stdout'},
        save: {type: 'stdout'},
        scarpingQueue: {type: 'stdout'},
        saveQueue: {type: 'stdout'},
        elementScarperXRay: {type: 'stdout'}
    },
    categories: {
        default: {appenders: ['out'], level: lvl},
        app: {appenders: ['out'], level: lvl},
        paginator: {appenders: ['paginator'], level: lvl},
        save: {appenders: ['save'], level: lvl},
        scarpingQueue: {appenders: ['scarpingQueue'], level: lvl},
        saveQueue: {appenders: ['saveQueue'], level: lvl},
        elementScarperXRay: {appenders: ['elementScarperXRay'], level: lvl}
    }
});

module.exports = log;