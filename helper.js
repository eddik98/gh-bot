module.exports = class Helper {

    removeDuplicates(arr) {
        let obj = {};

        arr.forEach((value) => {
            if (value !== undefined) {
                obj[value] = true;
            }
        });

        return Object.keys(obj);
    }

    static parseDate(data) {
        let month = {
            1: "января", 2: "февраля", 3: "марта",
            4: "апреля", 5: "мая", 6: "июня",
            7: "июля", 8: "августа", 9: "сентября",
            10: "октября", 11: "ноября", 12: "декабря"
        };

        let tmp = data.split(' ');
        let tmpNow = data.split(',');

        let date = 0;

        if (tmpNow[0] === 'Сегодня') {
            date = new Date();
        }

        if (tmp[2] !== undefined) {
            let tmp2 = tmp[2].split(',');

            for (key in month) {
                if (month[key] === tmp[1]) {
                    tmp[1] = key;
                    break;
                }
            }
            let tmp4 = tmp[0] + '-' + tmp[1] + '-' + tmp2[0];
            date = new Date(tmp4.replace(/(\d+)-(\d+)-(\d+)/, '$3-$2-$1'));
        }

        return date;
    }

    queue() {
        var first = 0;
        var last = -1;
        var q = [];
        var api = {};

        Object.defineProperties(api, {
            length: {
                get: function () {
                    return last - first + 1;
                }
            },

            enqueue: {
                value: function (val) {
                    last += 1;
                    q[last] = val;
                    return this.length;
                }
            },

            dequeue: {
                value: function () {
                    if (first <= last) {
                        var val = q[first];

                        delete q[first];
                        first += 1;

                        return val;
                    }
                }
            },

            peek: {
                value: function () {
                    return q[first];
                }
            },

            all: {
                value: function () {
                    return q;
                }
            }

        });

        return api;
    }

    async multithreading(array, func) {
        const promises = array.map(func);

        await Promise.all(promises);
    }

    is_url(str) {
        let regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
        return regexp.test(str);
    }
};