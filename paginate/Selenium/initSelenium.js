class initSelenium {

    constructor() {
        this.webdriver = require('selenium-webdriver');
        const chrome = require('selenium-webdriver/chrome');
        this.options = new chrome.Options();
        this.options.addArguments('headless');
        this.options.addArguments('disable-gpu');
        this.options.addArguments("--no-sandbox");
        this.options.addArguments("--blink-settings=imagesEnabled=false");

        chrome.setDefaultService(
            new chrome.ServiceBuilder(process.env.PWD + '/paginate/Selenium/driver/chromedriver').build()
        );
    }

    getDriver() {
        return new this.webdriver.Builder()
            .forBrowser('chrome')
            .withCapabilities(this.webdriver.Capabilities.chrome())
            .setChromeOptions(this.options)
            .build()
        //by: this.webdriver.By,
        //until: this.webdriver.until,
        //webdriver: this.webdriver
        //}
    }

}

module.exports = initSelenium;