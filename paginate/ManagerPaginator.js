const DataProvider = require('./../dataProvider/ManagerProvider').get();
const Paginator = require('./paginators/' + DataProvider.paginator);

module.exports = class ManagerPaginator {

    static get() {
        return Paginator;
    }

};