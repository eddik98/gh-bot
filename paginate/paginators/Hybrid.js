const x = require('x-ray')();
const l = require('../../config/logger').getLogger('paginator');
const initDriver = new (require('../Selenium/initSelenium'));
const DataProvider = require('./../../dataProvider/ManagerProvider').get();
const Helper = new (require('./../../helper'));

class Hybrid {

    constructor() {
        this.driver = initDriver.getDriver();
        this.provider = DataProvider;
    }

    /**
     * Инциализация алгоритма перехода по страницам
     *
     * @param {string} link
     * @param {function} cBackKue - Передача данных в Kue
     * @return {boolean, Error}
     */
    async init(link, cBackKue) {
        let totalPages;
        try {
            await this.driver.get(link);

            link = this.shapingLink(await this.driver.getCurrentUrl());

            totalPages = await this.totalPages();

            await this.driver.quit();

            await this.start(totalPages, link, cBackKue);

        } catch (e) {
            l.error(e, link);
            throw e;
        }
        return true;
    }

    /**
     * Запуск перехода по страницам
     *
     * @param {integer} totalPages
     * @param {string} link
     * @param {function} cBackKue
     * @return {boolean, Error}
     */
    async start(totalPages, link, cBackKue) {
        let result;
        let nowAttempts = 0;
        let attempts = 2;
        let totalAttempts = 0;

        for (let i = 1; i <= totalPages; i++) {
            try {
                let url = link + i;

                l.info('Страница:', i, url);
                result = await x(url, this.provider.getListLinks(x));

                result = this.validateLinks(result);

                cBackKue(result, i, totalPages);
            } catch (e) {

                if (nowAttempts !== attempts) {
                    l.error(e);
                    l.info('Попытка:', nowAttempts + 1, link);
                    i--;
                    nowAttempts++;
                } else if (nowAttempts === attempts) {
                    totalAttempts++;
                    nowAttempts = 0;
                }

                if (totalAttempts === attempts)
                    throw e;

            }
        }
        return true;
    }

    /**
     * Получение общего количества страниц
     *
     * @return {integer, Error}
     */
    async totalPages() {
        try {
            let totalPages = await this.driver.executeScript(
                this.provider.getTotalPages()
            );

            if (!totalPages)
                totalPages = 1;

            l.info('Общее кол. стр.:', totalPages);

            return totalPages;
        } catch (e) {
            throw e;
        }
    }

    /**
     * Формирование ссылки, для правильной работы,
     * проверяем есть ли вхождение в строке вида: /?
     *
     * @param {string} link
     * @return {string}
     */
    shapingLink(link) {
        let regexp = /\/(\?)/g;

        if (regexp.test(link)) {
            link = (this.provider.urlParams) ?
                link
                + '&' + this.provider.urlParams
                + '&' + this.provider.urlPrefix :
                link
                + '&' + this.provider.urlPrefix;

        } else {
            link = (this.provider.urlParams) ?
                link
                + '?' + this.provider.urlParams
                + '&' + this.provider.urlPrefix :
                link
                + '?' + this.provider.urlPrefix;

        }

        return link;
    }

    /**
     * Валидация полученых ссылок, на соответствие такому виду:
     * http(s)://...
     *
     * @param {array} links
     * @return {array}
     */
    validateLinks(links) {
        let tmp = [];
        links.forEach(link => {
            link = Helper.is_url(link) ? link : this.provider.url + link;
            tmp.push(link);
        });
        return tmp;
    }
}

module.exports = Hybrid;