const phantom = require('phantom');
const helper = require('../../helper');
const l = log.getLogger('paginator');

class Phantom {

    constructor(provider) {
        this.provider = provider;
        this.checkDuplicatesTmp = [];
        this.urls = [];
        this.currentPage = 1;
        this.totalPage = 1;
        this.prefix = this.provider.prefix;
        this.urlParams = this.provider.urlParams;
    }

    /**
     * Инициализация парсинга для обьявлений,
     * переход к следующей странице осуществляется по ссылке
     */
    async initAdvertLink(link) {
        try {
            var instance = await phantom.create(
                ['--load-images=no',
                    '--ignore-ssl-errors=true',
                    '--disk-cache=true'], {logLevel: 'warning'});

            let page = await instance.createPage();

            let status = await page.open(link);

            if (status !== 'success')
                throw new Error('Неудалось открыть ссылку: ' + status + ' | ' + this.url);

            this.totalPage = await this.totalPages(page);

            await this.getListUrls(page);

            for (let i = 2; i < this.totalPage; i++) {
                await this.walkPagesLinks(link + this.prefix + i, instance);
            }

            this.urls = helper.removeDuplicates(this.urls);
        } catch (e) {
            l.error(e);
        }

        instance.exit();
        return this.urls;
    }

    /**
     * Инициализация парсинга для товаров,
     * переход к следующей странице осуществляется по ссылке
     */
    async initGoodsLink(link) {
        l.debug('Категория:', link);
        try {
            var instance = await phantom.create(
                ['--load-images=no',
                    '--ignore-ssl-errors=true',
                    '--disk-cache=true'], {logLevel: 'warning'});

            let page = await instance.createPage();

            link = link + this.urlParams;

            let status = await page.open(link);

            if (status !== 'success')
                throw new Error('Неудалось открыть ссылку: ' + status + ' | ' + link);

            this.totalPage = await this.totalPages(page);

            //Сохраняем ссылки с первой страницы
            await this.getListUrlsGoods(page);
            l.debug('Страница:', 1);

            //Сохраняем ссылки с последующих страниц
            for (let i = 2; i <= this.totalPage; i++) {
                console.time('exe');
                //l.debug(link + this.prefix + i);
                l.debug('Страница:', i);
                await this.walkPagesLinks(link + this.prefix + i, instance);
                console.timeEnd('exe');
            }

        } catch (e) {
            l.error(e);
        }

        instance.exit();
        return this.urls;
    }

    /**
     * Получение общего количества страниц
     */
    async totalPages(page) {
        try {
            let totalPages = await page.evaluate(this.provider.getTotalPages());

            if (totalPages === 0)
                totalPages = 1;

            l.debug('Общее кол. стр.:', totalPages);

            return totalPages;
        } catch (e) {
            throw e;
        }
    }

    /**
     * Ходим по страницам
     */
    async walkPagesLinks(url, instance) {
        try {
            let page = await instance.createPage();

            let status = await page.open(url);

            if (status !== 'success')
                throw new Error('Неудалось открыть ссылку: ' + status);

            this.currentPage++;

            //await this.getListUrls(page);
            await this.getListUrlsGoods(page);
        } catch (e) {
            throw e;
        }
    }

    /**
     * Сохраняем все ссылки на странице
     */
    async getListUrls(page) {
        try {
            let rows = await page.evaluate(this.provider.getGoods());

            //Если не загрузилось с перового раза, пробуем еще раз
            if (rows.result === []) {
                rows = await page.evaluate(this.provider.getGoods());

                if (rows.result === [])
                    throw new Error('Отсутствуют товары');
            }

            rows = helper.removeDuplicates(rows);

            if (this.checkDuplicates(rows.result, this.checkDuplicatesTmp))
                throw new Error('Найдены повторяющиеся элементы. Стоп.');

            this.checkDuplicatesTmp = rows.result;

            let responseHook = this.hooks(rows);

            if (responseHook.status === false) {
                if (this.totalPage === 1)
                    this.urls = this.urls.concat(rows.result);

                throw responseHook.mes;
            }

            this.urls = this.urls.concat(rows.result);
        } catch (e) {
            throw e;
        }
    }

    /**
     * Сохраняем все ссылки на странице (модификация для товаров)
     */
    async getListUrlsGoods(page) {
        try {
            let rows = await page.evaluate(this.provider.getGoods());
            //l.debug(rows)
            //Если не загрузилось с перового раза, пробуем еще раз
            if (rows === [] || rows === null) {
                rows = await page.evaluate(this.provider.getGoods());

                if (rows === [] || rows === null)
                    throw new Error('Отсутствуют товары');
            }

            rows = helper.removeDuplicates(rows);

            if (this.checkDuplicates(rows, this.checkDuplicatesTmp))
                throw new Error('Найдены повторяющиеся элементы. Стоп.');

            this.checkDuplicatesTmp = rows;

            this.urls = this.urls.concat(rows);
        } catch (e) {
            throw e;
        }
    }

    checkDuplicates(rows, tmp) {
        let coincidences = 0;
        let maxCoincidences = 3;
        let percentCoincidences = 70;

        let uniq = (arrArg) => {
            return arrArg.filter((elem, pos, arr) => {
                if (!(arr.indexOf(elem) === pos)) {
                    //l.error('Найден дубликат:', elem);
                    coincidences++;
                }
            });
        };

        uniq(rows.concat(tmp));

        //l.error(tmp.length, rows.length);

        if (tmp.length !== 0) {
            let minLength = Math.min(tmp.length, rows.length);
            //l.error('minLength:', minLength);
            maxCoincidences = Math.floor((minLength * percentCoincidences) / 100);
            if (minLength === 1)
                maxCoincidences = 1;
        }

        //l.error('maxCoincidences:', maxCoincidences);
        //l.error('coincidences:', coincidences);

        return coincidences >= maxCoincidences;
    }

    hooks(data) {
        if (data.date === '')
            throw 'Пустая дата';

        let date = Helper.parseDate(data.date);
        let nowDate = new Date();
        let delta = nowDate.getTime() - date.getTime();
        let days = Math.floor(delta / 1000 / 60 / 60 / 24);

        if (days >= 7)
            if (this.currentPage !== 1 || this.totalPage === 1)
                return {status: false, mes: 'Количество дней привысило допустимое значение: ' + days};

        return {status: true, mes: 'Ok'};
    }

    /**
     * Ходим по стрницам, для пагинации по элементам страницы
     */
    async walkPagesElement(page) {
        for (let i = 1; i < this.totalPage; i++) {
            try {
                let result = await page.evaluate(this.provider.changePage(), i);

                l.info('Выбираем страницу:', i, 'Статус: ', result);

                await this.checkPageIsLoaded(page);

                await this.getListUrlsElement(page);

                this.currentPage++;
            } catch (e) {
                throw e;
            }
        }
    }

    /**
     * Для пагинации по элементам страницы
     */
    async checkPageIsLoaded(page) {
        let count = 5;
        let timeDelay = 3000;
        let timeInc = 1000;
        let status = false;

        for (let i = 0; i < count; i++) {
            try {
                status = await page.evaluate(
                    this.provider.pageIsLoaded()
                );

                l.info('Загрузилась ли страница?:');

                if (status === true) {
                    l.info('Да, загрузилась!');
                    break;
                } else {
                    l.info('Нет, не загрузилась!');
                }

                await setTimeout(() => {
                }, (timeDelay = timeDelay + timeInc));
            } catch (e) {
                throw e;
            }
        }

        if (status !== true) {
            this.savePoblemLink(this.url);
            throw 'Ошибка, страница не загрузилась, идем дальше!';
        }
    }
}

module.exports = Phantom;