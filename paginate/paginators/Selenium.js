const l = managerLibs.log.getLogger('paginator');

class Selenium {

    constructor(driver) {
        this.driver = driver;
        this.provider = managerLibs.dp;
        this.checkDuplicatesTmp = [];
        this.hlp = managerLibs.hlp;
    }

    async init(link, cbackSave) {
        try {
            await this.driver.get(link);

            link = await this.driver.getCurrentUrl() + this.provider.urlParams;

            this.link = link;

            let totalPage = await this.totalPages();

            for (let i = 1; i <= totalPage; i++) {
                l.debug('Страница:', i, link + this.provider.urlPrefix + i);

                await this.driver.get(link + this.provider.urlPrefix + i);

                try{
                    cbackSave(await this.getListUrlsGoods(), i, totalPage);
                }catch (e) {
                    l.info(e);
                }
            }

        } catch (e) {
            l.error(e);
            await this.driver.quit();
            cbackSave(await this.getListUrlsGoods(), totalPage, totalPage);
            return false;
        }

        await this.driver.quit();
        return true;
    }

    /**
     * Получение общего количества страниц
     */
    async totalPages() {
        try {
            let totalPages = await this.driver.executeScript(
                this.provider.getTotalPages()
            );

            if (!totalPages)
                totalPages = 1;

            l.debug('Общее кол. стр.:', totalPages);

            return totalPages;
        } catch (e) {
            throw e;
        }
    }

    /**
     * Сохраняем все ссылки на странице
     */
    async getListUrlsGoods() {
        try {
            let rows = await this.driver.executeScript(this.provider.getListUrls());

            //Если не загрузилось с перового раза, пробуем еще раз
            if (rows === [] || rows === null) {
                rows = await this.driver.executeScript(this.provider.getListUrls());

                if (rows === [] || rows === null)
                    throw new Error('Отсутствуют товары');
            }

            rows = this.hlp.removeDuplicates(rows);

            if (this.checkDuplicates(rows, this.checkDuplicatesTmp))
                throw new Error('Найдены повторяющиеся элементы. Стоп. | '+ this.link);

            this.checkDuplicatesTmp = rows;

            rows = this.validateUrls(rows);

            return rows;
        } catch (e) {
            throw e;
        }
    }

    validateUrls(arr) {
        let tmp = [];
        arr.forEach((elem) => {
            elem = this.hlp.is_url(elem) ? elem :this.provider.url + elem;
            tmp.push(elem);
        });
        return tmp;
    }

    checkDuplicates(rows, tmp) {
        let coincidences = 0;
        let maxCoincidences = 3;
        let percentCoincidences = 60;

        let uniq = (arrArg) => {
            return arrArg.filter((elem, pos, arr) => {
                if (!(arr.indexOf(elem) === pos)) {
                    //l.error('Найден дубликат:', elem);
                    coincidences++;
                }
            });
        };

        uniq(rows.concat(tmp));

        //l.error(tmp.length, rows.length);

        if (tmp.length !== 0) {
            let minLength = Math.min(tmp.length, rows.length);
            //l.error('minLength:', minLength);
            maxCoincidences = Math.floor((minLength * percentCoincidences) / 100);
            if (minLength === 1)
                maxCoincidences = 1;
        }

        //l.error('maxCoincidences:', maxCoincidences);
        //l.error('coincidences:', coincidences);

        return coincidences >= maxCoincidences;
    }
}

module.exports = Selenium;