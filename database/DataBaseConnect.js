const Sequelize = require('sequelize');
const Config = require('../config/config');

module.exports = class DataBaseConnect {

    constructor() {
        this.connect = this.getConnect();
        this.models = {};
        this.getModels();
        this.testConnect();
    }

    getConnect() {
        return new Sequelize(Config.database);
    }

    async closeConnect() {
        await this.connect.close();
    }

    getModels() {
        this.models.Goods = this.connect.import("./models/Goods/goods");
        this.models.DictionaryProperty = this.connect.import("./models/Goods/property_dictionary");
        this.models.PropertyGoods = this.connect.import("./models/Goods/property_goods");
        this.models.Image = this.connect.import("./models/Goods/goods_image");
        this.models.Category = this.connect.import("./models/Goods/category");
        this.models.Provider = this.connect.import("./models/Goods/providers");
        this.models.ListCategory = this.connect.import("./models/Goods/list_category");
    }

    testConnect() {
        this.connect.authenticate().catch(e => {
            console.error(e);
            process.exit();
        });
    }
};