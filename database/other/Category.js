const l = require('../../config/logger').getLogger();
const DataProvider = require('../../dataProvider/ManagerProvider').get();

module.exports = class Category extends require('./../main/Main'){

    constructor() {
        super();
        this.page = 1;
    }

    async init() {
        this.providerId = await DataProvider.getId();
    }

    async getLinks(limit) {
        try {
            let tmp = [];
            let options = {
                attributes: ['id', 'link'],
                limit: limit,
                offset: limit * (this.page - 1),
                where: {
                    provider_id: this.providerId
                }
            };

            if (process.env.ResumeWork === 'True')
                options.where.status = {[this.conn.Op.ne]: 'complete'};

            let result = await this.transaction(async (t) => {
                options['transaction'] = t;
                return await this.m.ListCategory.findAll(options);
            });

            this.page++;

            for (let i = 0; i < result.length; i++) {
                tmp.push(result[i].dataValues);
                await this.updateCatStatus(result[i].dataValues.id, 'processed');
            }

            return tmp;
        } catch (e) {
            l.error(e);
        }
    }

    async updateCatStatus(id, status) {
        try {
            await this.transaction(async (t) => {
                return await this.m.ListCategory.update({
                    status: status,
                    updated_at: Date.now()
                }, {
                    where: {id: id},
                    transaction: t
                });
            });
        } catch (e) {
            throw e;
        }
    }
};