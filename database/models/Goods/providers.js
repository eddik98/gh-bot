module.exports = function (sequelize, DataTypes) {
    return sequelize.define('providers', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(128),
            allowNull: false
        },
        link: {
            type: DataTypes.TEXT,
            allowNull: false
        }
    }, {
        tableName: 'providers',
        timestamps: false
    });
};