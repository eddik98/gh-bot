/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('list_category', {
        id: {
            type: DataTypes.INTEGER(12),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        provider_id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        link: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        status: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: "not_processed"
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: false
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        tableName: 'list_category',
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    });
};