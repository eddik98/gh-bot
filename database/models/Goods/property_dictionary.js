/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('property_dictionary', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'property_dictionary',
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  });
};