/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('goods_image', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        product_id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: true
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: false
        },
        link: {
            type: DataTypes.TEXT,
            allowNull: false
        }
    }, {
        tableName: 'goods_image',
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    });
};