module.exports = function (sequelize, DataTypes) {
    return sequelize.define('goods', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(255),
            allowNull: false,
            validate: {
                notEmpty: true,
            },
        },
        price: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            defaultValue: 0
        },
        description: {
            type: DataTypes.TEXT,
            allowNull: false,
            defaultValue: 'Описания нет'
        },
        category_id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            validate: {
                notEmpty: true,
            },
        },
        link: {
            type: DataTypes.TEXT,
            allowNull: false,
            validate: {
                notEmpty: true,
            },
        },
        currency_id: {
            type: DataTypes.INTEGER(2),
            allowNull: false,
            defaultValue: 1
        },
        meta_key: {
            type: DataTypes.TEXT,
            allowNull: false,
            defaultValue: ' '
        },
        rating: {
            type: DataTypes.FLOAT(11),
            allowNull: false,
            defaultValue: 0
        },
        meta_description: {
            type: DataTypes.TEXT,
            allowNull: false,
            defaultValue: ' '
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: false
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: false
        },
        keywords: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        provider_id: {
            type: DataTypes.INTEGER(3),
            allowNull: false
        }
    }, {
        tableName: 'goods',
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    });
};