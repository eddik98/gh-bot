/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('goods_status', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(30),
      allowNull: false
    }
  }, {
    tableName: 'goods_status',
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  });
};