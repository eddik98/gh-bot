/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('currency', {
    id: {
      type: DataTypes.INTEGER(3),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(30),
      allowNull: false
    }
  }, {
    tableName: 'currency',
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  });
};