/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('goods_image', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    product_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'goods',
        key: 'id'
      }
    },
    link: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    tableName: 'goods_image',
    timestamps: false
  });
};