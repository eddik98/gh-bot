module.exports = function (sequelize, DataTypes) {
    return sequelize.define('providers', {
        id: {
            type: DataTypes.INTEGER(3),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        link: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: false
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        tableName: 'provider',
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    });
};