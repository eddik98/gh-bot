/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('goods', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(255),
            allowNull: false,
            validate: {
                notEmpty: true,
            },
        },
        price: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        description: {
            type: DataTypes.TEXT,
            allowNull: false,
            validate: {
                notEmpty: true,
            },
        },
        category_id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'category',
                key: 'id'
            }
        },
        link: {
            type: DataTypes.TEXT,
            allowNull: false,
            validate: {
                notEmpty: true,
            },
        },
        currency_id: {
            type: DataTypes.INTEGER(2),
            allowNull: false,
            references: {
                model: 'currency',
                key: 'id'
            },
        },
        meta_key: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        meta_description: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: false
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: false
        },
        status_id: {
            type: DataTypes.INTEGER(3),
            allowNull: false,
            references: {
                model: 'goods_status',
                key: 'id'
            }
        },
        keywords: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        telephone: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        address: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        author: {
            type: DataTypes.STRING(180),
            allowNull: true
        },
        city: {
            type: DataTypes.STRING(180),
            allowNull: true
        },
        providers_id: {
            type: DataTypes.INTEGER(3),
            allowNull: false,
            references: {
                model: 'provider',
                key: 'id'
            }
        }
    }, {
        tableName: 'goods',
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    });
};