/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('property_goods', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    product_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'goods',
        key: 'id'
      }
    },
    property_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'property_dictionary',
        key: 'id'
      }
    },
    value: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    tableName: 'property_goods',
      timestamps: false
  });
};