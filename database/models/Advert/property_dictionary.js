/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('property_dictionary', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    lower_name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    russian_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'property_dictionary',
      timestamps: false
  });
};