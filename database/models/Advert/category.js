/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('category', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(255),
            allowNull: false,
            validate: {
                notEmpty: true,
            },
        },
        parent_id: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        providers_id: {
            type: DataTypes.INTEGER(3),
            allowNull: false,
            references: {
                model: 'provider',
                key: 'id'
            }
        }
    }, {
        tableName: 'category',
        timestamps: false
    });
};