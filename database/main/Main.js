const l = require('../../config/logger').getLogger('save');
const DBConnect = new (require('../DataBaseConnect'));

module.exports = class Main {

    constructor() {
        this.DBConnect = DBConnect;
        this.m = DBConnect.models;
        this.conn = DBConnect.connect;
    }

    async transaction(fn, isoLvl = 'SERIALIZABLE', test = null) {
        try {

            return await this.conn.transaction({
                isolationLevel: this.conn.Transaction.ISOLATION_LEVELS[isoLvl]
            }, fn);

        } catch (e) {

            if (e.hasOwnProperty('original') &&
                e.original.code === 'ER_LOCK_DEADLOCK'){

                if(test !== null)
                    //l.info(test);

                return await this.transaction(fn, isoLvl);
            } else {
                throw e;
            }
        }
    }

    async getOrSetProvider(name, link) {
        try {
            let result = await this.transaction(async (t) => {
                return await this.m.Provider.findOrCreate({
                    where: {
                        name: name,
                    },
                    defaults: {
                        name: name,
                        link: link
                    },
                    attributes: ['id'],
                    transaction: t,
                    lock: t.LOCK.UPDATE
                });
            }, 'READ_COMMITTED');

            return result[0].dataValues.id;
        } catch (e) {
            throw e;
        }
    }

    async listCategoryFindAll(providerId) {
        return await this.m.ListCategory.findAll({
            where: {
                provider_id: providerId
            },
            attributes: ['link']
        });
    }
};