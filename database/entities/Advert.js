const l = require('../../config/logger').getLogger('save');

class Advert {

    constructor(data, provider, db) {
        try {
            this.db = db;

            this.initModels();

            this.data = data;

            this.provider = provider;

            this.start().catch(e => {
                l.fatal(e);
            });
        } catch (e) {
            l.error(e);
        }
    }

    /**
     * Инициализация моделей
     */
    initModels() {
        this.Goods = this.db.import("../models/goods");
        this.DictionaryProperty = this.db.import("../models/property_dictionary");
        this.PropertyGoods = this.db.import("../models/property_goods");
        this.Image = this.db.import("../models/goods_image");
        this.Category = this.db.import("../models/category");
        this.Providers = this.db.import("../models/dataProvider");
    }

    async start() {
        try {
            let providerId = await this.providers(this.provider.name, this.provider.link);
            this.data.goods.providers_id = providerId;

            let categoryId = await this.categoryTree(this.data.category, providerId);

            let advert = await this.advert(this.data.goods, categoryId);
            if (advert[0]._options.isNewRecord === false)
                return false;
            let advertId = advert[0].dataValues.id;

            if (this.data.goods_image.length !== 0)
                await this.image(this.data.goods_image, advertId);

            if (this.data.property.length === 0)
                return true;

            let property = this.shapingProperty(this.data.property);

            await this.property(property.dictProperty, property.goodsProperty, advertId);
        } catch (e) {
            l.error(e);
        }
    }

    async providers(name, link) {
        try {
            let result = await this.Providers.findOrCreate({
                where: {
                    name: name,
                },
                defaults: {
                    name: name,
                    link: link
                }
            });

            return result[0].dataValues.id;
        } catch (e) {
            throw e;
        }
    }

    async categoryTree(category, providersId) {
        let id = 0;
        try {
            for (let i = 0; i < category.length; i++) {

                if (i === 0) {
                    //Есть ли в БД категория с таким именем?
                    let result = await this.Category.findOrCreate({
                        //Есть: сохраняем id
                        where: {
                            name: category[i].name,
                            providers_id: providersId
                        },
                        //Нет: Создаем и сохраняем id
                        defaults: {
                            name: category[i].name,
                            parent_id: id,
                            providers_id: providersId
                        }
                    });

                    id = result[0].dataValues.id;
                }

                //2 итерация и последующие
                if (i >= 1) {
                    //Проверяем parent_id, совпадает ли
                    //у найденной категории?
                    let result_2 = await this.Category.findOrCreate({
                        where: {
                            name: category[i].name,
                            providers_id: providersId,
                            parent_id: id,
                        },
                        //Нет: Создаем и сохраняем id
                        defaults: {
                            name: category[i].name,
                            parent_id: id,
                            providers_id: providersId
                        }
                    });

                    id = result_2[0].dataValues.id;
                }
            }

            return id;
        } catch (e) {
            throw e;
        }
    }

    async advert(data, categoryId) {
        try {
            this.Goods.hook('beforeValidate', (goods) => {
                goods.category_id = categoryId;
                goods.currency_id = 1;
                goods.status_id = 1;
                goods.keywords = '';

                if (!("name" in data))
                    throw new Error('Нету имени товара!');

                goods.price = ("price" in data) ? data.price : 0;
                goods.description = ("description" in data) ? data.description : 'Описание отсутствует';
            });

            this.Goods.hook('beforeCreate', (goods) => {
                goods.keywords = data.name.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '').split(' ');
                goods.keywords = JSON.stringify(goods.keywords);
            });

            let result = await this.Goods.findOrCreate({
                where: {
                    link: data.link,
                },
                defaults: data,
            });

            return result;
        } catch (e) {
            throw e;
        }
    }

    async image(data, id) {
        try {
            let goods_image = [];

            data.forEach((elem) => {
                elem.product_id = id;
                goods_image.push(elem);
            });

            await this.Image.bulkCreate(goods_image);

        } catch (e) {
            throw e;
        }
    }

    shapingProperty(data) {
        let dict_property = [];
        let goods_property = [];

        data.forEach((elem) => {
            let tmp = elem.value.split(':');
            dict_property.push({name: tmp[0]});
            goods_property.push({name: tmp[1]});
        });

        return {
            dictProperty: dict_property,
            goodsProperty: goods_property
        }
    }
    async property(dictProperty, goodsProperty, id) {
        try {

            for (let i = 0; i < dictProperty.length; i++) {

                let lowerName = dictProperty[i].name.toLowerCase();

                let dictProp = await this.DictionaryProperty.findOrCreate({
                    where: {
                        lower_name: lowerName,
                    },
                    defaults: {
                        name: dictProperty[i].name,
                        lower_name: lowerName
                    }
                });

                let dictPropId = dictProp[0].dataValues.id;

                await this.PropertyGoods.create({
                    product_id: id,
                    property_id: dictPropId,
                    value: goodsProperty[i].name,
                });
            }
        } catch (e) {
            throw e;
        }
    }
}

module.exports = Advert;
