const l = require('../../config/logger').getLogger('save');
const DataProvider = require('../../dataProvider/ManagerProvider').get();

module.exports = class SaveData extends require('../main/Main') {

    constructor() {
        super();
        this.provider = DataProvider;
    }

    /**
     * Запуск сохранения данных элемента
     *
     * @param {object} data
     * @return {boolean, Error}
     */
    async start(data) {
        try {
            let providerId = await this.provider.getId();

            let goodsId = await this.goodsCheck(data.goods.link);

            if (goodsId !== null) {
                await this.update(goodsId, providerId, data);

                return true;
            }

            await this.save(providerId, data);

            return true;
        } catch (e) {
            l.error(e);
            throw e;
        }
    }

    /**
     * Обновление данных элемента
     *
     * @param {integer} goodsId
     * @param {integer} providerId
     * @param {object} data
     * @return {boolean, Error}
     */
    async update(goodsId, providerId, data) {
        try {
            l.info('Обновляем элемент:', goodsId);

            if (data.goods_image.length !== 0)
                await this.redownloadImage(data.goods_image, goodsId);

            await this.updateProvider(providerId, goodsId);

            await this.updateDescription(data.goods.description, goodsId);

            await this.updateDate(goodsId);

        } catch (e) {
            throw e;
        }
    }

    /**
     * Сохранение данных элемента
     *
     * @param {integer} providerId
     * @param {object} data
     * @return {boolean, Error}
     */
    async save(providerId, data) {
        try {
            l.info('Сохраняем элемент:|', data.goods.link);

            data.goods.provider_id = providerId;

            let categoryId = await this.categoryTree(data.category, providerId);

            let goodsIdSave = await this.goods(data.goods, categoryId);

            let image = async () => {
                if (data.goods_image.length !== 0)
                    await this.image(data.goods_image, goodsIdSave);
            };

            let property = async () => {
                if (data.dict_property.length === 0)
                    return false;

                let tmp = [];

                for (let i = 0; i < data.dict_property.length; i++)
                    tmp.push(this.property(
                        data.dict_property[i],
                        data.goods_property[i],
                        goodsIdSave
                    ));

                await Promise.all(tmp);
            };

            await Promise.all([image(), property()]);
        } catch (e) {
            throw e;
        }
    }

    /**
     * Проверка существования элемента
     *
     * @param {string} link
     * @return {integer, Error}
     */
    async goodsCheck(link) {
        try {
            let result = await this.transaction(async (t) => {
                return await this.m.Goods.findOne({
                    where: {
                        link: link,
                    },
                    attributes: ['id'],
                    transaction: t
                });
            }, 'REPEATABLE_READ', 'goodsCheck');

            if (result !== null)
                return result.dataValues.id;

            return result;
        } catch (e) {
            throw e;
        }
    }

    /**
     * Проверка/сохранение элемента (товара)
     *
     * @param {object} data
     * @param {integer} categoryId
     * @return {integer, Error}
     */
    async goods(data, categoryId) {
        try {
            data = this.goodsDataShaping(data, categoryId);

            let goodsId = await this.transaction(async (t) => {
                return await this.m.Goods.findOrCreate({
                    where: {
                        link: data.link,
                    },
                    defaults: data,
                    transaction: t,
                    lock: t.LOCK.UPDATE
                });
            }, 'REPEATABLE_READ', data.name);

            return goodsId[0].id;
        } catch (e) {
            throw e;
        }
    }

    /**
     * Формирование данных элемента (товара)
     *
     * @param {object} data
     * @return {object, Error}
     */
    goodsDataShaping(data, categoryId) {
        data.category_id = categoryId;

        data.keywords = data.name
            .replace(/[&\/\\#\-,+()$~%.`'"!:*?<>{}]/g, '')
            .trim()
            .split(' ');

        data.keywords = JSON.stringify(data.keywords);

        return data;
    }

    /**
     * Формирование и сохранение древа категорий
     *
     * @param {array} data
     * @param {integer} providerId
     * @return {integer, Error}
     */
    async categoryTree(data, providerId) {
        let id = 0;
        let where;
        for (let i = 0; i < data.length; i++) {

            if (i === 0)
                where = {name: data[i], provider_id: providerId};
            else if (i >= 1)
                where = {
                    name: data[i], provider_id: providerId,
                    parent_id: id
                };

            id = await this.category(
                data[i],
                providerId,
                where,
                id);
        }

        return id;
    }

    /**
     * Сохранение древа категорий
     *
     * @param {string} category
     * @param {integer} providerId
     * @param {object} where
     * @param {number} parent_id
     * @return {integer, Error}
     */
    async category(category, providerId, where, parent_id) {
        try {
            let result = await this.transaction(async (t) => {
                return await this.m.Category.findOrCreate({
                    where: where,
                    defaults: {
                        name: category,
                        parent_id: parent_id,
                        provider_id: providerId
                    },
                    attributes: ['id'],
                    transaction: t,
                    lock: t.LOCK.UPDATE
                });
            }, 'REPEATABLE_READ', category);

            return result[0].dataValues.id;
        } catch (e) {
            throw e;
        }
    }

    /**
     * Сохранение ссылок на фото
     *
     * @param {array} data
     * @param {integer} goodsId
     * @return {Error}
     */
    async image(data, goodsId) {
        try {
            let goods_image = [];

            data.forEach((elem) => {
                goods_image.push({link: elem, product_id: goodsId});
            });

            await this.transaction(async (t) => {
                return await this.m.Image.bulkCreate(goods_image, {
                    transaction: t,
                    lock: t.LOCK.UPDATE
                });
            }, 'REPEATABLE_READ', 'image');
        } catch (e) {
            throw e;
        }
    }

    /**
     * Проверка/сохранение (название) параметра элемента (товара)
     *
     * @param {string} dict_property
     * @param {string} goods_property
     * @param {integer} goodsId
     * @return {Error}
     */
    async property(dict_property, goods_property, goodsId) {
        try {
            await this.transaction(async (t) => {
                let result = await this.m.DictionaryProperty.findOrCreate({
                    where: {
                        name: dict_property
                    },
                    defaults: {
                        name: dict_property
                    },
                    attributes: ['id'],
                    transaction: t,
                    lock: t.LOCK.UPDATE
                });

                return await this.goodsProperty(
                    goods_property,
                    goodsId,
                    result[0].dataValues.id,
                    t
                );
            }, 'REPEATABLE_READ', 'property');

        } catch (e) {
            throw e;
        }
    }

    /**
     * Сохранение (значения) параметра элемента (товара)
     *
     * @param {string} goods_property
     * @param {integer} goodsId
     * @param {integer} propertyId
     * @param t
     * @return {Error}
     */
    async goodsProperty(goods_property, goodsId, propertyId, t) {
        try {
            await this.m.PropertyGoods.create({
                product_id: goodsId,
                property_id: propertyId,
                value: goods_property,
            }, {transaction: t});
        } catch (e) {
            throw e;
        }
    }

    /**
     * Обновление ссылок уже сохраненных фото
     *
     * @param {array} data
     * @param {integer} id
     * @return {Error}
     */
    async redownloadImage(data, id) {
        try {
            let images = await this.transaction(async (t) => {
                return await this.m.Image.findAll({
                    where: {
                        product_id: id
                    },
                    attributes: ['id', 'link'],
                    transaction: t,
                    lock: t.LOCK.UPDATE
                });
            }, 'REPEATABLE_READ', 'redownloadImage');

            let tmp = [];

            for (let i = 0; i < images.length; i++) {
                if (data.length === i)
                    break;

                await this.transaction(async (t) => {
                    return await this.m.Image.update({
                        link: data[i],
                        updated_at: Date.now()
                    }, {
                        where: {id: images[i].dataValues.id},
                        transaction: t
                    });
                }, 'REPEATABLE_READ', 'Image.update');

                tmp = tmp.concat(data[i]);
            }

            if (images.length < data.length) {
                for (let i = 0; i < tmp.length; i++) {
                    let index = data.indexOf(tmp[i]);
                    if (index > -1)
                        data.splice(index, 1);
                }

                await this.image(data, id);
            }

        } catch (e) {
            throw e;
        }
    }

    /**
     * Обновление значения id провайдера, у элемента
     *
     * @param {integer} providerId
     * @param {integer} goodsId
     * @return {Error}
     */
    async updateProvider(providerId, goodsId) {
        try {
            await this.transaction(async (t) => {
                return await this.m.Goods.update({
                    provider_id: providerId
                }, {
                    where: {id: goodsId},
                    transaction: t
                });
            });
        } catch (e) {
            throw e;
        }
    }

    /**
     * Обновление значения поля update_at
     *
     * @param {integer} goodsId
     * @return {Error}
     */
    async updateDate(goodsId) {
        try {
            await this.transaction(async (t) => {
                return await this.m.Goods.update({
                    updated_at: Date.now()
                }, {
                    where: {id: goodsId},
                    transaction: t
                });
            });
        } catch (e) {

            throw e;
        }
    }

    /**
     * Обновление значения поля description
     *
     * @param {string} data
     * @param {integer} id
     * @return {Error}
     */
    async updateDescription(data, id) {
        try {
            await this.transaction(async (t) => {
                return await this.m.Goods.update({
                    description: data
                }, {
                    where: {id: id},
                    transaction: t
                });
            });
        } catch (e) {
            throw e;
        }
    }
};
