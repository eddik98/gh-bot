const SaveData = require('../database/entities/Goods');

class saveDataElement extends require('./main/jobs') {
    static async work(job, done) {
        try {
            let SD = new SaveData();
            await SD.start(job.data);
            done();
        } catch (e) {
            done(e);
        }
    }
}

module.exports = saveDataElement;