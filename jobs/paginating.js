const Paginator = require('../paginate/ManagerPaginator').get()
    , scarpingElement = require('./scarpingElement');

class paginating extends require('./main/jobs') {
    static async work(job, done) {
        try {
            await new Paginator().init(job.data.link, (links, currNumPage, totalPages) => {
                job.progress(currNumPage, totalPages);
                scarpingElement.createJob('scarpingElement', links);
            });

            done(null, job.data.id);
        } catch (e) {
            done(JSON.stringify({error: e.toString(), id: job.data.id}));
        }
    }
}

module.exports = paginating;