const Scarper = require('../scarper/ManagerScarper')
    , saveDataElement = require('./saveDataElement');

class scarpingElement extends require('./main/jobs') {
    static async work(job, done) {
        try {
            await Scarper.element.start(job.data, (result, currentNumElem, totalElem) => {
                job.progress(currentNumElem, totalElem);
                saveDataElement.createJob('saveDataElement', result, 'critical', 4);
            });

            done();
        } catch (e) {
            done(e);
        }
    }
}

module.exports = scarpingElement;