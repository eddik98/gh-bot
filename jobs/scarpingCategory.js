const Scarper = require('../scarper/ManagerScarper');

class scarpingCategory extends require('./main/jobs') {
    static async work(job, done) {
        try {
            await Scarper.category.start();

            done();
        } catch (e) {
            done(e);
        }
    }
}

module.exports = scarpingCategory;