const kue = require('kue')
    , config = require('../../config/config');

module.exports = class Jobs {

    static get Queue() {
        let queue =  kue.createQueue(config.redis).setMaxListeners(15);
        queue.watchStuckJobs(1000);
        return queue;
    }

    static createJob(name, data = {}, priority = 'normal', attempts = 2) {
        return Jobs.Queue.create(name, data)
            .priority(priority)
            .attempts(attempts)
            .backoff(true)
            .removeOnComplete(false)
            .save(err => {
                //if (err) l.error(queue.id);
            });
    }
};